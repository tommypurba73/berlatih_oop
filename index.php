<?php
@require_once 'animal.php';
@require_once 'ape.php';
@require_once 'frog.php';

$sheep = new Animal('shaun',2,'animal');
$sungokong = new ape('kerasakti',2,'auooo');
$kodok = new frog('buduk',4,'hop-hop');

echo $sheep->name; // "shaun"
echo"<br>";
echo $sheep->legs;// 2
echo"<br>";
echo $sheep->cold_blooded; // false
echo"<br>";
echo"<br>";

echo $sungokong->name; 
echo"<br>";
echo $sungokong->kaki;
echo"<br>";
echo $sungokong->yell; 
echo"<br>";
echo"<br>";

echo $kodok->name; 
echo"<br>";
echo $kodok->feet;
echo"<br>";
echo $kodok->jump; 
echo"<br>";
echo"<br>";

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>